/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Models.Compound;
import Models.Element;
import Models.Group;
import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Damian
 */
public class MainClassTest {

    @Before
    public void init() {
        Element o2 = new Element("O", 0, 2);    //O2
        Element cl2 = new Element("Cl", 0, 2);  //Cl2
        Element cl = new Element("Cl", -1, 1);  //Cl-  
        Element mg2 = new Element("Mg", 2, 1);  //Mg2+
        Element mg = new Element("Mg", 0, 1);   //Mg
        Group oh = new Group();                 //OH-
        oh.getElements().add(new Element("O", -2, 1));
        oh.getElements().add(new Element("H", 1, 1));
        oh.setOxidationLevel(-1);
        Group so4 = new Group();                //(SO4)2-
    }

    // testing test
    @Test
    public void add() {
        int x = 1;
        int y = 2;
        int result = x + y;
        assert result == 3 : "should be 3";
    }

    @Test
    public void setIndexes(Element left, Element right) {
        //... + E -> E + ...
        int leftNumber = left.getLowerIndex() * left.getPreIndex();
        int rightNumber = right.getLowerIndex() * right.getPreIndex();

        if (leftNumber > rightNumber) {

        } else if (leftNumber < rightNumber) {

        }

    }

}
