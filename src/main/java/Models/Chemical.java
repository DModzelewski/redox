/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Damian
 */
public class Chemical {

    private int preIndex;

    public int getPreIndex() {
        if (preIndex < 1) {
            return 1;
        } else {
            return preIndex;
        }
    }

    public void setPreIndex(int preIndex) {
        this.preIndex = preIndex;
    }

}
