package Models;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class representing a single group
 * @author Damian
 */
public class Group extends Chemical{
    private List<Element> elements = new ArrayList<>();
    private int oxidationLevel;
    private int lowerIndex;

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    public int getOxidationLevel() {
        return oxidationLevel;
    }

    public void setOxidationLevel(int oxidationLevel) {
        this.oxidationLevel = oxidationLevel;
    }

    public int getLowerIndex() {
        if (lowerIndex < 1) {
            return 1;
        } else {
            return lowerIndex;
        }
    }

    public void setLowerIndex(int lowerIndex) {
        this.lowerIndex = lowerIndex;
    }

    /**
     * 
     * @return returns Symbols and indexes of elements, if group has index
     * on its own then it's returned in brackets
     */
    @Override
    public String toString() {
        
      String elementsString = elements.stream().map(n -> String.valueOf(n))
              .collect(Collectors.joining(""));
        
        if (lowerIndex<2){
            return elementsString+"";
        }else {
            return "("+elementsString+")"+lowerIndex;
        }
    }
    
    
}
