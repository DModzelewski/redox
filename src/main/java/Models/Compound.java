package Models;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Damian
 */
public class Compound extends Chemical{
    private List<Chemical> elements = new LinkedList<>();

    public List<Chemical> getElements() {
        return elements;
    }

    public void setElements(List<Chemical> elements) {
        this.elements = elements;
    }

    /**
     * 
     * @return returns whole compound in valid format
     */
    @Override
    public String toString() {
        
         String elementsString = elements.stream().map(n -> String.valueOf(n))
              .collect(Collectors.joining(""));
        
        return elementsString + "";
    }
   
    
}
