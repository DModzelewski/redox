package Models;

import com.sun.xml.internal.ws.util.StringUtils;

/**
 * Class representing a single element
 *
 * @author Damian
 */
public class Element extends Chemical {

    private String symbol;
    private int oxidationLevel;
    private int lowerIndex;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getOxidationLevel() {
        return oxidationLevel;
    }

    public void setOxidationLevel(int oxidationLevel) {
        this.oxidationLevel = oxidationLevel;
    }

    public int getLowerIndex() {
        if (lowerIndex < 1) {
            return 1;
        } else {
            return lowerIndex;
        }
    }

    public void setLowerIndex(int lowerIndex) {
        this.lowerIndex = lowerIndex;
    }

    public Element(String symbol, int oxidationLevel, int lowerIndex) {
        this.symbol = symbol;
        this.oxidationLevel = oxidationLevel;
        this.lowerIndex = lowerIndex;
    }

    public Element() {
    }

    public Element(String symbol, int oxidationLevel) {
        this.symbol = symbol;
        this.oxidationLevel = oxidationLevel;
    }

    /**
     *
     * @return returns Symbol and also index if it's higher than 1. Also
     * capitalizes the first letter
     */
    @Override
    public String toString() {

        symbol = symbol.toLowerCase();
        symbol = StringUtils.capitalize(symbol);

        if (lowerIndex <= 1) {
            return symbol;
        } else {
            return symbol + lowerIndex;
        }
    }

}
