
import Models.Chemical;
import Models.Compound;
import Models.Element;
import Models.Group;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MainClass {

    // Declaring sides of equations
    private static List<Chemical> leftSide = new LinkedList<>();
    private static List<Chemical> rightSide = new LinkedList<>();

    // Declaring oxidizer and reducer for both sides and groups they are in
    private static Element leftOxidizer;
    private static Group leftOxidizerGroup;
    private static Element rightOxidizer;
    private static Group rightOxidizerGroup;
    private static Element leftReducer;
    private static Group leftReducerGroup;
    private static Element rightReducer;
    private static Group rightReducerGroup;

    public static void main(String[] args) {

        /*
         * Declaring variables: running - specifies if program is running or
         * terminated scanner - for input
         */
        boolean running = true;
        Scanner scanner = new Scanner(System.in);

        // Running the program
        System.out.println("Welcome text");
        while (running) {

            int command;
            do {
                System.out.println("1-Add an element (SUBSTRAT) \n"
                        + "2-Add a compound (SUBSTRAT) \n"
                        + "3-Add an element (PRODUCT) \n"
                        + "4-Add a compound (PRODUCT) ");
                if (!leftSide.isEmpty() && !rightSide.isEmpty()) {
                    System.out.println("5-Check equation");
                    System.out.println("6-EXECUTE");
                }
                command = notString(scanner);
                switch (command) {
                    case 1: {
                        leftSide.add(addElement(scanner));
                        break;
                    }
                    case 2: {
                        leftSide.add(addCompound(scanner));
                        break;
                    }
                    case 3: {
                        rightSide.add(addElement(scanner));
                        break;
                    }
                    case 4: {
                        rightSide.add(addCompound(scanner));
                        break;
                    }
                    case 5: {
                        if (!leftSide.isEmpty() && !rightSide.isEmpty()) {
                            for (int i = 0; i < leftSide.size() - 1; i++) {
                                System.out.print(leftSide.get(i) + " + ");
                            }
                            System.out.print(leftSide.get(leftSide.size() - 1) + " -> ");
                            for (int i = 0; i < rightSide.size() - 1; i++) {
                                System.out.print(rightSide.get(i) + " + ");
                            }
                            System.out.println(rightSide.get(rightSide.size() - 1));
                        } else {
                            System.out.println("Wrong input");
                            command = 0;
                        }
                        break;
                    }
                    case 6: {
                        if (!leftSide.isEmpty() && !rightSide.isEmpty()) {
                            System.out.println("Executing;");
                            execute(leftSide, rightSide);
                        } else {
                            System.out.println("Wrong input");
                            command = 0;
                        }
                        break;
                    }
                    default: {
                        System.out.println("Wrong input");
                        break;
                    }
                }
            } while (command != 6);
        }
    }

    /**
     * Method for adding elements
     *
     * @param scanner our scanner
     * @return returns created element
     */
    public static Element addElement(Scanner scanner) {
        Element e = new Element();
        System.out.println("Symbol");
        e.setSymbol(scanner.next());
        System.out.println("Lower index");
        e.setLowerIndex(notString(scanner));
        System.out.println("Oxidation level");
        e.setOxidationLevel(notString(scanner));
        System.out.println("Your element is : " + e);
        return e;
    }

    /**
     * Method for adding a group of elements
     *
     * @param scanner our scanner
     * @return created group
     */
    public static Group addGroup(Scanner scanner) {
        Group g = new Group();
        int command = 0;
        do {
            System.out.println("1-Add element to the group");
            if (!g.getElements().isEmpty()) {
                System.out.println("2-Save group");
            }
            command = notString(scanner);
            switch (command) {
                case 1: {
                    g.getElements().add(addElement(scanner));
                    break;
                }
                case 2: {
                    if (g.getElements().isEmpty()) {
                        System.out.println("Wrong input");
                        command = 0;
                    } else {
                        System.out.println("Group lower index");
                        int index = notString(scanner);
                        g.setLowerIndex(index);
                        System.out.println("Saved");
                    }
                    break;
                }
                default: {
                    System.out.println("Wrong input");
                    break;
                }
            }

        } while (command != 2);
        System.out.println("Your group is : " + g);
        return g;
    }

    /**
     * Method for adding a full compound
     *
     * @param scanner our scanner
     * @return created compound
     */
    public static Compound addCompound(Scanner scanner) {
        Compound c = new Compound();
        int command = 0;
        do {
            System.out.println("1-Add element to the compound \n"
                    + "2-Add group to the compound");
            if (!c.getElements().isEmpty()) {
                System.out.println("3-Save compound");
            }
            command = notString(scanner);
            switch (command) {
                case 1: {
                    c.getElements().add(addElement(scanner));
                    break;
                }
                case 2: {
                    c.getElements().add(addGroup(scanner));
                    break;
                }
                case 3: {
                    if (c.getElements().isEmpty()) {
                        System.out.println("Wrong input");
                        command = 0;
                    } else {
                        System.out.println("Saved");
                    }
                    break;
                }
                default: {
                    System.out.println("Wrong input");
                    break;
                }
            }

        } while (command != 3);
        System.out.println("Your compound is : " + c);
        return c;
    }

    /**
     * Checks if user inputs a number, not a string
     *
     * @param scanner our nice scanner object
     * @return int we want from user
     */
    public static int notString(Scanner scanner) {
        String input;
        int outputInt = -999;
        while (outputInt == -999) {
            input = scanner.next();
            try {
                outputInt = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println("Input a number please");
            }
        }
        return outputInt;
    }

    public static void execute(List<Chemical> leftSide, List<Chemical> rightSide) {

        /* 
        big ugly loop, I'm still not good enough with stream() methods
         */
        for (Chemical cL : leftSide) {    // main loop - iterating through every Chemical (cL) on the left side of equation
            if (cL instanceof Compound) {    //check if first chemical is a Compound
                for (Chemical cLElements : ((Compound) cL).getElements()) {  //iterating through left side's chemical elements
                    if (cLElements instanceof Element) {  //check if element of left side's compound is an Element
                        iterateRight(cLElements, rightSide); //secondary loop
                    } else if (cLElements instanceof Group) {    //check if element of left side's compound is a Group of Elements
                        for (Element cLGroupElements : ((Group) cLElements).getElements()) {
                            iterateRight(cLGroupElements, rightSide);//secondary loop
                        }
                    } else {
                        System.out.println("Error");
                    }
                }
            } else if (cL instanceof Element) {   //check if first chemical is an Element
                iterateRight(cL, rightSide); //secondary loop
            } else {
                System.out.println("ERROR");
            }
        }

        findGroups();
        printOxidizerAndReducer();

    }

    public static void iterateRight(Chemical cL, List<Chemical> rightSide) {
        for (Chemical cR : rightSide) {  // secondary loop - iterating first Chemical (cL) through each right side's of equation Chemical (cR)
            if (cR instanceof Compound) { //check if second chemical is a Compound
                for (Chemical cRElements : ((Compound) cR).getElements()) {   //iterating through elements of right side Compound (cR)
                    if (cRElements instanceof Element) {  //check if cR element is actual Element
                        if (((Element) cL).getSymbol().equals(((Element) cRElements).getSymbol())) {  //check if element cL and Element of cR have the same symbol
                            if (((Element) cL).getOxidationLevel() > ((Element) cRElements).getOxidationLevel()) {  //check if their oxidation level differ
                                leftOxidizer = (Element) cL;                                                      //basicly: increased level on the right side means
                                rightOxidizer = (Element) cRElements;                                                     //that chemical is a Reducer, if decreased it's Oxidizer
                            } else if (((Element) cL).getOxidationLevel() < ((Element) cRElements).getOxidationLevel()) {  //if level is unchanged then we leave this element
                                leftReducer = (Element) cL;
                                rightReducer = (Element) cRElements;
                            }
                        }
                    } else if (cRElements instanceof Group) {//check if cR element is a Group
                        for (Element cRGroupElement : ((Group) cRElements).getElements()) {  //iterating through Right Compound group's elements
                            if (((Element) cL).getSymbol().equals(((Element) cRGroupElement).getSymbol())) {  //check if element cL and Element of cR have the same symbol
                                if (((Element) cL).getOxidationLevel() > ((Element) cRGroupElement).getOxidationLevel()) {  //check if their oxidation level differ
                                    leftOxidizer = (Element) cL;                                                      //basicly: increased level on the right side means
                                    rightOxidizer = cRGroupElement;                                                     //that chemical is a Reducer, if decreased it's Oxidizer
                                } else if (((Element) cL).getOxidationLevel() < ((Element) cRGroupElement).getOxidationLevel()) {  //if level is unchanged then we leave this element
                                    leftReducer = (Element) cL;
                                    rightReducer = cRGroupElement;
                                }
                            }
                        }
                    }
                }
            } else if (cR instanceof Element) {  //check if second chemical is an Element
                if (((Element) cL).getSymbol().equals(((Element) cR).getSymbol())) {  //check if element cL and cR have the same symbol
                    if (((Element) cL).getOxidationLevel() > ((Element) cR).getOxidationLevel()) {  //check if their oxidation level differ
                        leftOxidizer = (Element) cL;                                                      //basicly: increased level on the right side means
                        rightOxidizer = (Element) cR;                                                     //that chemical is a Reducer, if decreased it's Oxidizer
                    } else if (((Element) cL).getOxidationLevel() < ((Element) cR).getOxidationLevel()) {  //if level is unchanged then we leave this element
                        leftReducer = (Element) cL;
                        rightReducer = (Element) cR;
                    }
                }
            } else {
                System.out.println("Error");
            }
        }
    }

    /**
     * Method to check if oxidizer or reducer is in some group, so it could be
     * passed to the valid half-reaction
     */
    public static void findGroups() {

        for (Chemical c : leftSide) { // First for the every chemical of Left Side of equation
            if (c instanceof Compound) {    // check if chemical is a compound so it can contain groups
                for (Chemical g : ((Compound) c).getElements()) {   //checks every chemical of compound looking for a group
                    if (g instanceof Group) {
                        if (((Group) g).getElements().contains(leftOxidizer)) {
                            leftOxidizerGroup = (Group) g;
                        } else if (((Group) g).getElements().contains(leftReducer)) {
                            leftReducerGroup = (Group) g;
                        }
                    }
                }
            }
        }

        for (Chemical c : rightSide) {    // Right side of equation
            if (c instanceof Compound) {
                for (Chemical g : ((Compound) c).getElements()) {
                    if (g instanceof Group) {
                        if (((Group) g).getElements().contains(rightOxidizer)) {
                            rightOxidizerGroup = (Group) g;
                        } else if (((Group) g).getElements().contains(rightReducer)) {
                            rightReducerGroup = (Group) g;
                        }
                    }
                }
            }
        }
    }

    /**
     * Method for printing Oxidizer and Reducer as a single element or whole
     * group it belongs to
     */
    public static void printOxidizerAndReducer() {


        System.out.println("Reduction:");
        
        //  Printing oxidizer oxidation level
        System.out.println(leftOxidizer.getOxidationLevel() + " -------> " + rightOxidizer.getOxidationLevel());
        
       //  Printing left side oxidizer with electrons       
        Element oxyElectron = new Element("E-",-1);
             
        if (leftOxidizerGroup != null) {
                    oxyElectron.setPreIndex((leftOxidizer.getOxidationLevel()-rightOxidizer.getOxidationLevel())*leftOxidizer.getLowerIndex()*leftOxidizerGroup.getLowerIndex());      
           System.out.print(leftOxidizerGroup + " + " + oxyElectron.getPreIndex()+ "" + oxyElectron + " -> ");
        } else if (leftOxidizerGroup == null) {
                    oxyElectron.setPreIndex((leftOxidizer.getOxidationLevel()-rightOxidizer.getOxidationLevel())*leftOxidizer.getLowerIndex());      
            System.out.print(leftOxidizer + " + " + oxyElectron.getPreIndex() + "" + oxyElectron + " -> ");
        }
        //  Printing right side oxidizer
        if (rightOxidizerGroup != null) {
            System.out.print(rightOxidizerGroup);
        } else if (rightOxidizerGroup == null) {
            System.out.print(rightOxidizer);
        }

        System.out.println("");
        System.out.println("");


        System.out.println("Oxidation:");
        
        //  Printing reducer oxidation level
        System.out.println(leftReducer.getOxidationLevel() + " -------> " + rightReducer.getOxidationLevel());
       
        //  Printing left side reducer
        
        Element redElectron = new Element("E-",-1);
        
        if (leftReducerGroup != null) {
            System.out.print(leftReducerGroup + " -> ");
        } else if (leftReducerGroup == null) {
            System.out.print(leftReducer + " -> ");
        }
        //  Printing right side reducer and electrons
        if (rightReducerGroup != null) {
             redElectron.setPreIndex((rightReducer.getOxidationLevel()-leftReducer.getOxidationLevel())*leftReducer.getLowerIndex()*leftReducerGroup.getLowerIndex());    
            System.out.print(rightReducerGroup +" + "+ redElectron.getPreIndex() + "" + redElectron);
        } else if (rightReducerGroup == null) {
            redElectron.setPreIndex((rightReducer.getOxidationLevel()-leftReducer.getOxidationLevel())*leftReducer.getLowerIndex());    
            System.out.print(rightReducer +" + "+ redElectron.getPreIndex() + "" + redElectron);
        }

        System.out.println("");
        
        
    }
}
